NOTE: Requires GCC 6.2.0 or later for compilation. Order versions of the GCC
compiler will not handle this code correctly. Compilse using command:

    gcc -o align aling.c

 Run using:

    ./align file.fa

I compiled on Ubuntu 16.04. If there are any compilation issues or other,
system-related hiccups, let me know.

I went with traditional Needleman-Wunsch alignment due to the sheer difficulty
of implementing the Hirschberg variant in C. The program first reads in each
sequence, then flood-fills the requisite dynamic programming matrix. From there,
by taking the maximum score from the above, left or diagonal (up and left)
neighboring matrix cell (starting from the maximum alignment score residing
at the bottom-rightmost entry of DP matrix and pushing upwards to the
top-leftmost cell). For each cell discovered this way, the algorithm adds
the appropriate character for each sequence that produced this alignment score
(contributed to the path to the maximum alignment score). Given this is done
starting from the end of the alignment, the program then reverses the strings
and finally re-scores the alignment before outputting both the maximum alignment
score and the relevant pairwise alignment.
