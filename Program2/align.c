/*
 * Sean Corbett
 * 03/24/2017
 * CSCI 332: Algorithms
 * Program 2: Pairwise Sequence Alignment
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * I've implemented the traditional Needleman-Wunsch algorithn here, which
 * runs in O(mn) time using O(mn) space. The Hirschberg optimzation does
 * reduce the amount of space to O(min[m,n]), however the time complexity
 * remains O(mn). I focused on getting Needleman-Wunsch right first before
 * implementing the significantly more complex optimizations Hirschberg
 * devised.
 *
 * NOTE: Credit where credit is due. I took major inspiration for this
 * implementation from the following python repo:
 *
 * https://github.com/alevchuk/pairwise-alignment-in-python/blob/master/alignment.py#L83
 */
const int gapPenalty = -5;
void getSequences(char *sys_in, char *seq1[], char *seq2[]);
int *floodMatrix(int *score_mtx, char *seq1, char *seq2);
void traceback(int *score_mtx, char *seq1, char *seq2, char *opt1[], char *opt2[]);
void revAndScore(char *opt1, char *opt2);
int score(char a, char b);
int mismatch(char a, char b);
int max(int a, int b, int c);
char *revStr(char *stringIn);
int calcScore(char *opt1, char *opt2);

/*
 * Main executes all main functions. Unlike the previous
 * assignment, I tried to minimize arbitrary functions and localize
 * work to each specific function (as opposed to having a larger, more
 * convoluted function attempt to manage several portions of a
 * program).
 */
int main(int argc, char **argv){
    int *score_mtx;
    char *seq1;
    char *seq2;
    char *opt1;
    char *opt2;
    int m = 0;
    int n = 0;

    // Load fasta sequence data
    getSequences(argv[1] ,&seq1, &seq2);
    // I tried to get pass by reference to work, this flood-fills our DP matrix.
    score_mtx = floodMatrix(score_mtx,seq1,seq2);
    // Once our max score occupies the bottom left corner, follow back using
    // traditional Needleman-Wunsch DP method.
    traceback(score_mtx,seq1,seq2,&opt1,&opt2);
    // The sequences produced from our traceback are reversed and
    // unscored. This function corrects the sequences using the
    // revString helper function, and then scores the alignments
    // using the calcScore function before outputting the score and
    // aligned sequences.
    revAndScore(opt1, opt2);



    return 0;
}

/*
 * Load sequences from input fasta file into each sequence string.
 */
void getSequences(char *sys_in, char *seq1[], char *seq2[]){
    char *fileName = sys_in;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    FILE *file;
    file = fopen(fileName,"r");
    int seqNum = 0;

    if(file == NULL){
       exit(EXIT_FAILURE);
    }

    while((read = getline(&line, &len, file)) != -1){
      if(seqNum == 0){
        *seq1 = malloc(read * sizeof(char));
        strcpy(*seq1,line);
      }
      else{
        *seq2 = malloc(read * sizeof(char));
        strcpy(*seq2,line);
      }
      seqNum++;
    }
}

/*
 * floodMatrix isn't a particularly remarkable function, however
 * I did choose to utilize a dimensionally-reduced 2d array (2d->1d)
 * for the memory access speedup and ease of use.
 */
int *floodMatrix(int *score_mtx, char *seq1, char *seq2){
  int m = (int)strlen(seq1);
  int n = (int)strlen(seq2);

  score_mtx = malloc((m + 1) * (n + 1) * sizeof(int));
  int i;
  int j;
  score_mtx[0] = 0;

  for(j=0; j<n; j++){
    score_mtx[j] = gapPenalty * j;
  }
  for(i=0; i<m; i++){
    score_mtx[i * n] = gapPenalty * i;
  }
  for(i=1; i<m; i++){
    for(j=1; j<n; j++){
        int match = score_mtx[(i-1) * n + (j-1)] + score(seq1[i-1],seq2[j-1]);
        int delete = score_mtx[(i-1) * n + j ] + gapPenalty;
        int insert = score_mtx[i * n + (j-1)] + gapPenalty;
        score_mtx[i * n + j] = max(match,delete,insert);
    }
  }

  return score_mtx;
}

/*
 * Score helper function determines whether letters input are matched,
 * mismatched, or gapped. From there, applies appropriate penalty.
 */
int score(char a, char b){
  if(a == b){
    return 2;
  }
  else if(a == '-' || b == '-'){
    return gapPenalty;
  }
  else{
    return mismatch(a,b);
  }
  return 0;
}

/*
 * I decided to give letter mismatches their own separate function so that
 * the score didn't become too unweildy.
 */
int mismatch(char a, char b){
  if(a == 'A'){
    if(b == 'T'){
      return -1;
    }
    return -5;
  }
  else if(a == 'T'){
    if(b == 'A'){
      return -1;
    }
    return -5;
  }
  else if(a == 'C'){
    if(b == 'G'){
      return -1;
    }
    return -5;
  }
  else if(a == 'G'){
    if(b == 'C'){
      return -1;
    }
    return -5;
  }
}

/*
 * NOTE: I took this from the below:
 * http://stackoverflow.com/questions/7074010/find-maximum-of-three-number-in-c-without-using-conditional-statement-and-ternar
 *
 * I found the function particularly succinct, and a good way of getting the
 * max of three elements.
 */
int max(int a, int b, int c)
{
     int m = a;
     (m < b) && (m = b); //these are not conditional statements.
     (m < c) && (m = c); //these are just boolean expressions.
     return m;
}

/*
 * Short of correctly flood-filling the matrix, tracing back the correct path
 * of a pairwise alignment matrix represents the major share of work done by
 * Needleman-Wunsch. I used temp strings so pass by reference would work, and
 * figuring out how to correctly navigate through the matrix was a *huge*
 * challenge.
 */
void traceback(int *score_mtx, char *seq1, char *seq2, char *opt1[], char *opt2[]){
  *opt1 = malloc(((int)strlen(seq1)+1) * sizeof(char) * 1000);
  *opt2 = malloc(((int)strlen(seq2)+1) * sizeof(char) * 1000);
  char *opt1temp = malloc(((int)strlen(seq1)+1) * sizeof(char) * 1000);
  char *opt2temp = malloc(((int)strlen(seq2)+1) * sizeof(char) * 1000);
  int m = (int)strlen(seq1);
  int n = (int)strlen(seq2);

  int i = m-1;
  int j = n-1;
  int pos1 = 0;
  int pos2 = 0;

  while(i > 0 && j > 0){
    int current = score_mtx[i * n + j];
    int diag = score_mtx[(i-1) * n + (j-1)];
    int up = score_mtx[(i-1) * n + j];
    int left = score_mtx[i * n + (j-1)];
    if(current == diag + score(seq1[i-1],seq2[j-1])){
      opt1temp[pos1] = seq1[i-1];
      opt2temp[pos2] = seq2[j-1];
      i--;
      j--;
    }
    else if(current == left + gapPenalty){
      opt1temp[pos1] = '-';
      opt2temp[pos2] = seq2[j-1];
      j--;
    }
    else if(current == up + gapPenalty){
      opt1temp[pos1] = seq1[i-1];
      opt2temp[pos2] = '-';
      i--;
    }
    pos1++;
    pos2++;

  }

  while(i > 0){
    opt1temp[pos1] = seq1[i-1];
    opt2temp[pos2] = '-';

    pos1++;
    pos2++;
    i--;
  }

  while(j > 0){
    opt1temp[pos1] = '-';
    opt2temp[pos2] = seq2[j-1];

    pos1++;
    pos2++;
    j--;
  }

  strcpy(*opt1,opt1temp);
  strcpy(*opt2,opt2temp);

}

/*
 * Now that we have our optimal alignment, we must un-reverse the strings
 * and score the resultant pairwise alignment.
 */
void revAndScore(char *opt1, char *opt2){
  revStr(opt1);
  revStr(opt2);
  int score = calcScore(opt1,opt2);

  // Print our result, we're outta here!
  printf("Score: %d\n", score);
  printf("%s\n",opt1);
  printf("%s\n",opt2);
}

/*
 * Reverse each output alignment string.
 */
char *revStr(char *stringIn){
  int m = (int)strlen(stringIn);
  int end = m-1;
  for(int i=0;i<(m/2); i++){
    char temp = stringIn[i];
    stringIn[i] = stringIn[end];
    stringIn[end] = temp;
    end--;
  }
  return stringIn;
}

/*
 * Rather than reinventing the wheel, I used a similar system to when
 * calculating the score for a matrix, reusing my score and mismatch functions.
 */
int calcScore(char *opt1, char *opt2){
  int m = (int)strlen(opt1);
  int i;
  int totalScore = 0;

  for(i=0; i<m; i++){
    if(opt1[i] == opt2[i]){
      totalScore += score(opt1[i],opt2[i]);
    }
    else if(opt1[i] != opt2[i] && opt1[i] != '-' && opt2[i] != '-'){
      totalScore += mismatch(opt1[i],opt2[i]);
    }
    else if(opt1[i] == '-' || opt2[i] == '-'){
      totalScore += gapPenalty;
    }
  }

  return totalScore;
}

void subAlign(int *subMtx, int boundA, int boundB, char *seq1, char *seq2){

}
