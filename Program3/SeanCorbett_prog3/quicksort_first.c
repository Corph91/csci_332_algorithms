/* Sean Corbett
 * 04/26/2017
 * CSCI332: Algorithms
 * Program3: Quicksort first
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Put function tags up here for easy to read code
void loadArray(int *arrayIn, int *arraySize, char *FileIn);
void quicksort(int *arrayIn, int left, int right);
int calcPart(int *arrayIn, int left, int right);
void swapNums(int *a, int *b);
void writeToFile(int *arrayIn, int arraySize, char *FileOut);
int comparisons = 0;

/*
 * Main calls loadArray, initial quicksort, and
 * writeToFile functions, as well as seeds
 * rand.
 */
int main(int argc, char **argv){
  int *array = malloc(sizeof(int) * 1000);
  int actualSize;
  loadArray(array, &actualSize, argv[1]);
  int i;

  quicksort(array, 0, actualSize-1);
  printf("%d comparisons made\n", comparisons);

  writeToFile(array, actualSize, argv[2]);
  free(array);

  return 0;   
}

/*
 * Function loads integers from file int array of ints
 * passed by reference. Also pass and set arraySize
 * by reference for convenience.
 */
void loadArray(int *arrayIn, int *arraySize, char *FileIn){
  (*arraySize) = 0;
  FILE *fileIn = fopen(FileIn,"r");
  if(fileIn == NULL){
    exit(EXIT_FAILURE);
  }

  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  
  int numIn;
  int i = 0;
  int inSize = 1000;


   while((read = getline(&line, &len, fileIn)) != -1){
       (*arraySize)++;
       sscanf(line, "%d", &numIn);
       arrayIn[i] = numIn;
       i++;
       if(i > inSize){
           arrayIn = realloc(arrayIn, inSize * 100);
           inSize *= 100;
       }
   }

   fclose(fileIn);

}

/*
 * Main quicksort funciton makes recursive calls to self
 * with partition calculated in calcPart() function below.
 * I wanted to keep the partition calculation and quicksort
 * recursion separate so code would be easier to read and
 * less prone to bugs. Note that I increment the number of 
 * comparisons here at the beginning. I made comparisons
 * a global int due to pass-by-reference error.
 */
void quicksort(int *arrayIn, int left, int right){
  int partition;
  comparisons += (right - left);

  if(left < right){
      partition = calcPart(arrayIn, left, right);
      quicksort(arrayIn,left,partition-1);
      quicksort(arrayIn,partition+1,right);
  }
}

/*
 * Most important function of quicksort. Calculates
 * partitions as first element of input array, and
 * then compares and swaps values until val at left
 * mark is > than val at right mark, then swaps marks.
 */
int calcPart(int *arrayIn, int left, int right){
  int index = left;
  int pivot = arrayIn[index];
  int i = left - 1;
  int j;

  swapNums(&arrayIn[index], &arrayIn[right]);

  for(j=left; j<right; j++){
      if(arrayIn[j] < pivot){
          i++;
          swapNums(&arrayIn[i],&arrayIn[j]);
      }
  }

  swapNums(&arrayIn[i+1], &arrayIn[right]);
  return i + 1;
}

/*
 * Simple swapping function made sepearate
 * to keep code readable.
 */
void swapNums(int *a, int *b){
 int temp;
 temp = *a;
 *a = *b;
 *b = temp;
}

/*
 * Simple function to write each element of sorted
 * array to a line of argument output file. Kept
 * separate from main function to improve code
 * readability.
 */
void writeToFile(int *arrayIn, int arraySize, char *FileOut){
    FILE *fileOut = fopen(FileOut,"w");
    
    if(fileOut == NULL){
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<arraySize; i++){
        fprintf(fileOut,"%d\n",arrayIn[i]);
    }
    fclose(fileOut);
}