Sean Corbett
04/26/2017
CSCI332
Program 3: Quicksort

NOTE: Compile using GCC 6.2.0 or later with command:

    gcc -o quicksort_first quicksort_first.c

for the quicksort_first program and:

    gcc -o quicksort_random quicksort_random.c

for the quicksort_random program. Both files accept two arguments

    quicksort <FILEIN.txt> <FILEOUT.out>

but print the number of comparisons used to console. I used the
following example code for reference: 

    http://www.sanfoundry.com/c-program-implement-quick-sort-using-randomization/

particularly for the partitioning function. Note that the randomized
implementation does not guarantee consistent runtime or number of 
comparisons, but will regularly achieve better performance than the
first-element pivot implementation.