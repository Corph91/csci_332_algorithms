/*
 * Sean Corbett
 * 02/28/2017
 * CSCI 332
 * Homework 3
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Initialize constants and structs. Note here that I'm using an adjacency
 * list structure. It's the most efficient graph representation for the task
 * at hand. The graph struct also stores two such structures, one being the
 * graph in reverse, the other being the normal graph.
 *
 * NOTE: I took a good deal of inspiration for the graph generation code from
 * geeksforgeeks's adjacency list example.
*/
int time = 0;
int idCount = 0;
int* topTen;
int increment = 0;

struct Stack{
    int* visitOrder;
    int size;
    int memSize;
};

struct Node{
    int toNode;
    int visited;
    int discoveryTime;
    int finishTime;
    int SCCID;
    struct Node* nextNode;
};

struct List{
    int size;
    int visited;
    int fromNode;
    int SCCID;
    struct Node* headNode;
};

struct Graph{
    int size;
    int memSize;
    struct List* nodeList;
    struct List* revList;
    int edgeNum;

};

struct SCC{
    int size;
    int id;
};

/*
 * There's a long list of functions I use to accomplish the goal of
 * finding the largest SCC sizes. Obviously we want to be able to make
 * nodes and edges easily, as well as call our depth-first seach recusrively.
 * I've also included some utility functions (i.e. push and pop) to help clean
 * up the code.
 *
*/
struct Node* MakeNode(int toNodeNum);
void MakeEdge(int thisNode, int nextNode);
struct Graph* createGraph(char fileName[100]);
void SCCSeach();
void DFS();
struct List visit(struct List nodeIn);
void reverseGraph();
void push(int nodeIn);
void DFSR();
struct List visitR(struct List nodeIn);
struct SCC makeComponent(int idCount);
int compare (const void* p1, const void* p2);
void fillTop(struct SCC component);
int pop();
int cmpfunc (const void * a, const void * b);

struct SCC* SCClist;
struct Graph* myGraph;
struct Stack* myStack;

/*
 * Originally I was hyper-conservative with memory allocation. This turned
 * out to break loading the graph completely on the larger input. I've
 * struck a compromize here by somewhat conservatively allocating memory
 * to begin, but allowing the reallocation of said memory to be much more
 * aggressive;
 *
*/
int main(int argc, char *argv[]){
    char fileName[100];
    char ch;
    char command[100];
    strcpy( command, "ulimit -s unlimited");
    system(command);

    myStack = (struct Stack*)malloc(sizeof(struct Stack));
    myStack->memSize = 100;
    myStack->visitOrder = malloc(myStack->memSize * sizeof(int));
    myStack->size = 0;
    SCClist = malloc(100 * sizeof(struct SCC));
    topTen = malloc(10 * sizeof(int));

    sscanf(argv[1],"%s %[^ /t]",fileName,&ch);
    myGraph = (struct Graph*)malloc(sizeof(struct Graph));
    myGraph = createGraph(fileName);
    SCCSeach();

    qsort(topTen,10,sizeof(int),cmpfunc);
    idCount = (idCount < 10) ? idCount : 10;
    for(int i=0; i<idCount; i++){
	printf("%d,", topTen[i]);
    }
    printf("\n");
    return 0;
}

// Initializes nodes;
struct Node* MakeNode(int toNodeNum){
	struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
	newNode->toNode = toNodeNum;
	newNode->nextNode = NULL;
	newNode->visited = 0;
	return newNode;	
}

/*
 * This function was a wall of code to begin with, and it got moreso when I
 * added in the reverse graph. This essentially takes two integers from input
 * creates nodes from both, and then assigns pointer "edges" for both normal
 * graph and graph in reverse.
*/
void MakeEdge(int thisNode, int nextNode){
 	struct Node* newNode = MakeNode(nextNode);
	if(myGraph->nodeList[thisNode].headNode == NULL){
	    myGraph->nodeList[thisNode].headNode = (struct Node*)malloc(sizeof(struct Node));
	    myGraph->nodeList[thisNode].headNode->visited = 0;
	    myGraph->nodeList[thisNode].size = 0;
	    myGraph->nodeList[thisNode].visited = 0;
	    myGraph->nodeList[thisNode].fromNode = thisNode;
	    myGraph->nodeList[thisNode].SCCID = 0;
	}
	myGraph->nodeList[thisNode].fromNode = thisNode;
	newNode->nextNode = myGraph->nodeList[thisNode].headNode;
	myGraph->nodeList[thisNode].headNode = newNode;
	myGraph->nodeList[thisNode].size += 1;

	struct Node* newRevNode = MakeNode(thisNode);
	if(myGraph->revList[nextNode].headNode == NULL){
	    myGraph->revList[nextNode].headNode = (struct Node*)malloc(sizeof(struct Node));
	    myGraph->revList[nextNode].headNode->visited = 0;
	    myGraph->revList[nextNode].size = 0;
	    myGraph->revList[nextNode].visited = 0;
	    myGraph->revList[nextNode].fromNode = nextNode;
	    myGraph->revList[nextNode].SCCID = 0;
	}
	myGraph->revList[nextNode].fromNode = nextNode;
	newRevNode->nextNode = myGraph->revList[nextNode].headNode;
	myGraph->revList[nextNode].headNode = newRevNode;
	myGraph->revList[nextNode].size += 1;
}

/*
 * Other than the "MakeEdge" functiontion, the "createGraph" fucntion
 * involves the most upfront code. it reads in the file (given a filename)
 * reads in file data, creates a graph using the makeEdge function, and returns
 * a pointer to the completed graph once done. Note that memory is dynamically
 * reallocated here based upon incoming data size.
 */
struct Graph* createGraph(char fileName[100]){
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int nodeFrom;
    int nodeTo;
    int NumEdges = 0;

    myGraph->size = 1;
    myGraph->memSize = 100;
    myGraph->nodeList = malloc(myGraph->memSize * sizeof(struct Node));
    myGraph->revList = malloc(myGraph->memSize * sizeof(struct Node));
    FILE *file;
    file = fopen(fileName,"r");
   
    if(file == NULL){
	exit(EXIT_FAILURE);
    }

    while((read = getline(&line, &len, file)) != -1){
	if(nodeFrom && nodeTo > 0){
		sscanf(line, "%d %d", &nodeFrom, &nodeTo);
		if(nodeFrom > myGraph->memSize || nodeTo > myGraph->memSize){
		    int increase = (nodeFrom > nodeTo) ? nodeFrom : nodeTo;
		    myGraph->nodeList = realloc(myGraph->nodeList, increase * 10 * sizeof(struct Node));
		    myGraph->revList = realloc(myGraph->revList, increase * 10 * sizeof(struct Node));
		    myGraph->memSize = increase * 10;
		}
		if(nodeFrom > myGraph->size){
		  myGraph->size = nodeFrom;
		}
		else if(nodeTo > myGraph->size){
		  myGraph->size = nodeTo;
		}
		MakeEdge(nodeFrom, nodeTo);
		NumEdges+=1;
	}
    }
    fclose(file);
    return myGraph;
}

/*
 * I wanted a "master function" to call the two DFS functions.
 * Given each DFS function works a bit differently, I thought
 * it would make more sense to keep the reverse and SCC DFS passes
 * as independent functions.
*/
void SCCSeach(){ 
    DFS();
    DFSR();
}

/*
 * This took a while to perfect. I took notes from the depth-first search
 * Python algorithm Yolanda uses from Pythonds (for data structures) and
 * went from there. Otherwise, it's a standard, recursive DFS algorithm,
 * though the recursive part is kept in a seperate function below.
 */
void DFS(){
    for(int i=1; i<=myGraph->size;i++){
	if(myGraph->revList[i].visited == 0){
	    myGraph->revList[i].visited = 1;
	    if(myGraph->revList[i].size > 0){
		myGraph->revList[i] = visit(myGraph->revList[i]);
	    }
	    push(i);
	}	    
    }
}

/*
 * One thing I like about the Pythonds implementation is that it keeps
 * the recursive part of the algorithm seperate. I think this leads to
 * cleaner code. The reasoning behind the for loop is for tighter control
 * over number of iterations. I was having to resort to an ungodly long
 * conditional with a while loop to get it to work half as well.
 */
struct List visit(struct List nodeIn){	
    struct Node* neighbor = nodeIn.headNode;
    for(int i=1; i<=nodeIn.size; i++){
        if(myGraph->revList[neighbor->toNode].visited == 0){
            myGraph->revList[neighbor->toNode].visited = 1;
            myGraph->revList[neighbor->toNode] = visit(myGraph->revList[neighbor->toNode]);
            push(neighbor->toNode);
        }
	neighbor = neighbor->nextNode;	
    }
    nodeIn.visited = 2;
    return nodeIn;
}

/*
 * This is the second part of Kosaraju's algorithm. Once the initial
 * pass on the reverse graph is complete, when then perform DFS on the
 * normal graph, but in reverse postorder of the stack. To accomplish 
 * this, I use the a "pop" function, which assigns to an iterator variable.
 * SCC number (idCount) helps fill an array of SCC struct types.
 *
 * Note that each SCC consists of a size and and id. It's a relatively
 * simple structure, but it helps keep things nice and tidy.
 */
void DFSR(){
    int SCCsize = sizeof(SCClist)/sizeof(SCClist[0]);
    int i;
    while((i = pop()) != -1){
	if(myGraph->nodeList[i].visited == 0){
	    idCount++;
	    myGraph->nodeList[i].SCCID = idCount;
	    myGraph->nodeList[i].visited = 1;
	    if(idCount > SCCsize){
		SCClist = realloc(SCClist, SCCsize * 10 * sizeof(struct SCC));
		SCCsize = SCCsize * 10;
	    }
	    SCClist[idCount] = makeComponent(idCount);
	    SCClist[idCount].size += 1;
	    if(myGraph->nodeList[i].size > 0){
		myGraph->nodeList[i] = visitR(myGraph->nodeList[i]);
	    }
	    fillTop(SCClist[idCount]);
	}	    
    }
}

/*
 * Much as with the prior DFS function, I've kept the recursive part
 * seperate. There's not much difference here between the first DFS
 * and the second DFS's "visit" function, just that it assigns each
 * visited neighbor node the parent node's SCC id and increments
 * the related SCC's size by one for each adjacent node to a parent.
 */
struct List visitR(struct List nodeIn){	
    struct Node* neighbor = nodeIn.headNode;
    while(neighbor->nextNode !=NULL){
	    if(myGraph->nodeList[neighbor->toNode].visited == 0){
	        myGraph->nodeList[neighbor->toNode].SCCID = nodeIn.SCCID;
	        myGraph->nodeList[neighbor->toNode].visited = 1;
	        SCClist[idCount].size += 1;
	        myGraph->nodeList[neighbor->toNode] = visitR(myGraph->nodeList[neighbor->toNode]);
	    }
	    neighbor = neighbor->nextNode; 
    }       
    nodeIn.visited = 2;
    return nodeIn;
}

/*
 * Like the "MakeNode" function this initializes an
 * SCC structure to initial values. However, rather than a 
 * pointer it returns the SCC itself (which is then stored)
 * in the global SCC list.
 */
struct SCC makeComponent(int idCount){
    struct SCC newSCC;
    newSCC.size = 0;
    newSCC.id = idCount;

    return newSCC;
}

/*
 * This function got a good deal more complex than I intended. It
 * increases the size of the stack and then sets the input node id
 * to the next availible slot. To accomplish this given an unknown
 * number of node requires dynamic memory reallocation though.
 */
void push(int nodeIn){
    myStack->size = myStack->size + 1;
    if(myStack->memSize < nodeIn){
	myStack->memSize = nodeIn * 10;
	myStack->visitOrder = realloc(myStack->visitOrder,nodeIn * 10 * sizeof(int));
    }
    if(myStack->visitOrder[myStack->size] < 0){
	myStack->visitOrder[myStack->size] = 0;
    }
    myStack->visitOrder[myStack->size] = nodeIn;
}

/*
 * This is you run of the mill pop function for a 
 * stack. It takes the gloval stack, pops off the
 * topmost value, and decrements size by one. If the stack
 * is empty, it returns -1.
 */
int pop(){
   if(myStack->size > 0){
	int popped = myStack->visitOrder[myStack->size];
	myStack->size = myStack->size -1;
	return popped;
   }
   else{
	return -1;
   }
}

/*
 * Rather than sorting through all the SCC sizes post-computation
 * like an idiot, I though it would be smart to calculate the top
 * ten as SCC's are identified.
 */
void fillTop(struct SCC component){
    if(idCount < 10){
	topTen[increment] = component.size;
	increment++;
    }
    else{
	int minimum = topTen[0];
	int min_index = 0;
	for(int i=0; i<10; i++){
	    if(topTen[i] < minimum){
		minimum = topTen[i];
		min_index = i;
	    }
	}
	topTen[min_index] = (component.size > topTen[min_index]) ? component.size : minimum;
    }
}
int cmpfunc (const void * a, const void * b){

   return (*(int*)b - *(int*)a);
}
