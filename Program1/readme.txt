Sean Corbett
02/28/2017
CSCI 332
Homework 3

The following program reads in a a .txt file of n nodes. Each row is parsed (by space) and the two numbers
within each row are stored in two integers. The integers are then passed to a MakeEdge function, which creates
two nodes (one for each number) and an "edge" (pointer) between them. The program simultaneously constructs
the transpose of the graph in like manner as input is read. Once all nodes have been read in and all
edges made, the program then proceeds to call depth-first-search on the transposed graph, pushing nodes
to a stack data structure in order of finish time. Once completed, depth-first-seach is then called again,
however nodes are visited in reverse postorder, thus nodes are popped off the stack from the back. During
this second depth first search, a global list of strongly connected components and a SCC id variable are
used. The SCC id increments each time a new node must be popped off a stack, and a new SCC is created with
this id (and subsequently stored in the the SCC list). As nodes are recursively visited, the size of a given
SCC is incremented. Once all nodes from a stack-popped node have been visited, the SCC is sent to a function
and compared to the sizes of the ten largest existing SCC's. If its size is greater than any of the ten largest
SCCs, the SCC's size replaces the smallest SCC size currently stored among the top ten. Finally, the DFS concludes,
and the largest ten SCC sizes are printed.
